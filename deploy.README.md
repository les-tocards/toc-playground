<!-- TODO : Rewrite, Improve, Do it in Englsh -->
<!-- TODO : Files description -->

- Créer un nouveau droplet sur DigitalOcean avec l'image Docker
- Ajouter sa clé SSH pour pouvoir y accéder
- SSH en tant que root sur le droplet

<!-- Export/Source $IP, $USER -->

## Création de l'utilisateur

```sh
# Connexion au serveur avec le compte root via SSH
ssh root@$IP

# Créer un nouvel utilisateur
adduser $USER

# Ajouter l'utilisateur au groupe sudo
usermod -aG sudo newuser

# Switcher vers ce nouvel utilisateur
su - $USER

# Créer le dossier pour le SSH
mkdir ~/.ssh

# Éditer le fichier suivant pour y coller la clé publique SSH de votre PC
nano ~/.ssh/authorized_keys

# Mettre les bonnes permissions sur ce fichier
chmod 600 ~/.ssh/authorized_keys

# Se déconnecter
exit
```

## Configuration pour Gitlab

```sh
# Connexion au serveur avec le compte utilisateur via SSH
ssh $USER@$IP

# Création d'une nouvelle pair de clés ssh
# Laisser tous les champs vide (taper entrée)
ssh-keygen -t rsa

# Afficher la clé publique pour copier tout son contenu
cat .ssh/id_rsa.pub

# Éditer le fichier suivant pour y coller la clé publique SSH
nano ~/.ssh/authorized_keys

# Afficher le contenu de la clé privé (ATTENTION, DONNÉE SENSIBLE)
cat ~/.ssh/id_rsa

# Renseigner les variables suivantes dans Gitlab
# Repository > Settings > CI/CD > Variables
DEPLOY_SERVER_IP_ADDRESS  = $IP
DEPLOY_SERVER_PRIVATE_KEY = ...

# Supprimer la limite SSH du pare-feu
sudo ufw allow 22/tcp
```

## Configuration pour Docker

```sh
# Ajouter l'utilisateur au groupe docker
# Afin de pouvoir lancer des commandes docker sans sudo
sudo gpasswd -a $USER docker

# Redémarrer le service docker
sudo service docker restart
```
