#!/bin/bash
#
# Rename files to defaults to avoid specifying them in commands
#
mv ./deploy.docker-compose.yml ./docker-compose.yml
mv ./deploy.env ./.env
#
# Execute docker-compose commands
#
docker-compose pull
docker-compose up -d
#
echo "==========[ Deployment Result ]=========="
docker ps